unit uFraItemLista;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants, 
  FMX.Types, FMX.Graphics, FMX.Controls, FMX.Forms, FMX.Dialogs, FMX.StdCtrls,
  FMX.Ani, FMX.Objects, FMX.Layouts, FMX.Controls.Presentation;

type
  TfraItem = class(TFrame)
    Layout1: TLayout;
    Rectangle1: TRectangle;
    Rectangle2: TRectangle;
    Layout2: TLayout;
    RectAnimation1: TRectAnimation;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent; Titulo: string); reintroduce;
  end;

implementation

{$R *.fmx}

{ TFrame2 }

constructor TfraItem.Create(AOwner: TComponent; Titulo: string);
begin
  inherited Create(AOwner);
  Label1.Text := Titulo;
end;

end.
