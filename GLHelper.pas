unit GLHelper;

interface

uses
  FMX.Layouts;
type
  TGridLayoutHelper = class helper for TGridLayout
  procedure Clear;
  end;

implementation

{ TGridLayoutHelper }

procedure TGridLayoutHelper.Clear;
//var
//  I: Integer;
begin
//  for I := 0 to Self.ControlsCount - 1 do
//  begin
//    Self.Controls[I].DeleteChildren;
//  end;
  Self.DeleteChildren;
end;

end.
