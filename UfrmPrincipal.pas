unit UfrmPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Layouts,
  FMX.Effects, FMX.Controls.Presentation, FMX.StdCtrls, GLHelper;

type
  TForm1 = class(TForm)
    ToolBar1: TToolBar;
    ShadowEffect1: TShadowEffect;
    VertScrollBox1: TVertScrollBox;
    GridLayout1: TGridLayout;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure GridLayout1Painting(Sender: TObject; Canvas: TCanvas;
      const ARect: TRectF);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  uFraItemLista;

{$R *.fmx}

procedure TForm1.GridLayout1Painting(Sender: TObject; Canvas: TCanvas;
  const ARect: TRectF);
begin
//  GridLayout1.Height := GridLayout1.ItemHeight * GridLayout1.ControlsCount;
end;

procedure TForm1.SpeedButton1Click(Sender: TObject);
var
  I: Integer;
  Frame: TfraItem;
begin
//  GridLayout1.BeginUpdate;
  GridLayout1.Clear;
//  Sleep(200);
  for I := 1 to 50 do
  begin
    Frame := TfraItem.Create(Self, 'Titulo '+ i.ToString);
    Frame.Parent := GridLayout1;
    Frame.Name := 'Frame' + i.ToString;
    Frame.Tag := I;
    Frame.Margins.Top := 5;
    Frame.Margins.Bottom := 5;
    GridLayout1.Height := GridLayout1.ItemHeight * GridLayout1.ControlsCount;
  end;
//  GridLayout1.EndUpdate;

end;

procedure TForm1.SpeedButton2Click(Sender: TObject);
begin
  GridLayout1.Clear;
  GridLayout1.Repaint;
end;

end.
